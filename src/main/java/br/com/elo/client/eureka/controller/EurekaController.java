package br.com.elo.client.eureka.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
@RequestMapping("/v1/client")
public class EurekaController {
	
	@Value("${api.eureka.client.totalRegistros:0}")
	private Integer totalRegistros;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getEmissorPorFiltro() {
        return ResponseEntity.ok(totalRegistros);
    }
}
